const http = require('http');
const url = require('url');
const { v4: uuidv4 } = require('uuid');

const server = http.createServer((req, res) => {
    const parsedUrl = url.parse(req.url, true);
    const path = parsedUrl.pathname;

    if (path === '/html') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(`<!DOCTYPE html>
                    <html>
                        <head>
                        </head>
                        <body>
                            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                            <p> - Martin Fowler</p>
                        </body>
                    </html>`);
    } else if (path === '/json') {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify({
            "slideshow": {
                "author": "Yours Truly",
                "date": "date of publication",
                "slides": [
                    {
                        "title": "Wake up to WonderWidgets!",
                        "type": "all"
                    },
                    {
                        "items": [
                            "Why <em>WonderWidgets</em> are great",
                            "Who <em>buys</em> WonderWidgets"
                        ],
                        "title": "Overview",
                        "type": "all"
                    }
                ],
                "title": "Sample Slide Show"
            }
        }));
    } else if (path === '/uuid') {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify({
            "uuid": uuidv4()
        }));
    } else if (path.match('/status/')) {
        const statusCode = parseInt(path.split('/')[2]);
        if (statusCode >= 100 && statusCode <= 599) {
            if (statusCode >= 104 && statusCode <= 199) {
                res.write('Invalid Status Code');
            } else if (statusCode >= 100 && statusCode <= 103) {
                res.writeHead(200, { 'Content-Type': 'text/plain' });
                res.write(`Status Code: ${statusCode}`);
            } else {
                res.writeHead(statusCode, { 'Content-Type': 'text/plain' });
                res.write(`Status Code: ${statusCode}`);
            }
        } else {
            res.write(`Invalid Status Code`);
        }
    } else if (path.match('/delay/')) {
        const delayInSeconds = parseInt(path.split('/')[2]);
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        if (isNaN(delayInSeconds)) {
            res.write('Invalid format of seconds');
        } else {
            setTimeout(() => {
                res.write(`Response after ${delayInSeconds} seconds`);
                res.end();
            }, delayInSeconds * 1000);
            return;
        }
    } else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.write('Error: 404 Not Found');
    }
    res.end();
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});